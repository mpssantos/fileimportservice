File Import Microservice
==============================================================================================
Author: Marco Santos

Technologies: SpringBoot, Java 8, RabbitMQ, Consul

What is it?
-----------

This is the FileImport Microservice, part of the microservices architecture SalesCloud POC

Several objects (Deal, Lead, etc.) can be exported from csv files into the system. When the file import is done, a message is sent indicating that an import was created and can now be processed. 

Typically, dealfileimport or leadfileimport would be able to listen to the listen the message "fileimport.%s.created" and process the respective import data.   

System requirements
-------------------

All you need to build this project is Java (perhaps the latest version), Maven 3.1 or better.
This microservice is dependent of an already existent infrastructure with:
* Consul (Service Registry)
* RabbitMQ (AMQP message broker)

 
Configure Maven
---------------

If you have not yet done so, you must [Configure Maven] https://maven.apache.org/


Run the Tests
-------------------------
The tests can be executed from the command line as follow:

    mvn clean test

or to have a report

    mvn clean test surefire-report:report site

Tests Results Reports
-------------
The test results reports can be found on the following folder:

/target/site/surefire-report.html


Code Coverage Reports
-------------
The code coverage report is generated when we use the maven "package" goal:

    mvn clean package

The report can be found in the following folder:
/target/site/surefire-report.html

