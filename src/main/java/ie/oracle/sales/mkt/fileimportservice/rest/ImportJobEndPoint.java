package ie.oracle.sales.mkt.fileimportservice.rest;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import ie.oracle.sales.mkt.fileimportservice.mapper.ImportJobBeanMapper;
import ie.oracle.sales.mkt.fileimportservice.mapper.MapperException;
import ie.oracle.sales.mkt.fileimportservice.rabbitmq.EventProducerException;
import ie.oracle.sales.mkt.fileimportservice.rabbitmq.ImportJobEventsProducer;
import ie.oracle.sales.mkt.fileimportservice.service.ImportJobService;
import ie.oracle.sales.mkt.fileimportservice.service.ServiceException;
import ie.oracle.sales.salescloudutils.rest.JSONUtil;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by mapasant on 07/10/2016.
 */
@Component
@Path("/importjobs")
public class ImportJobEndPoint {
    private static Logger logger = Logger.getLogger(ImportJobEndPoint.class);

    @Autowired
    private ImportJobService importJobService;

    @Autowired
    private ImportJobBeanMapper importJobBeanMapper;

    @Autowired
    private ImportJobEventsProducer importJobEventsProducer;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImportJobs() {
        logger.info("Get Import Jobs List operation invoked");
        List<ImportJob> importJobList = importJobService.findAllImportJobs();

        try {
            List<ImportJobBean> importJobBeanList = importJobBeanMapper.mapList(importJobList);
            String jsonString = JSONUtil.toJson(importJobBeanList);
            return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
        } catch (MapperException e) {
            e.printStackTrace();
        }

        return Response.serverError().entity("There was a problem retrieving import jobs list").build();
    }

    @GET
    @Path("/{importJobId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImportJobById(@PathParam("importJobId") long importJobId) {
        logger.info(String.format("Get Import Job operation invoked for id [%d]", importJobId));

        ImportJob importJob = new ImportJob();
        importJob.setImportJobId(importJobId);
        //importJob.setSysTaskId(6005555L);
        importJob.setName(String.format("BP MSANTOS %d", importJobId));

        String jsonString = JSONUtil.toJson(importJob);
        return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response createImportJob(@FormDataParam("fileImportJob") String  fileImportJob,
                                    @FormDataParam("file") InputStream fileInputStream,
                                    @FormDataParam("file") FormDataContentDisposition fileMetaData) {

        logger.info(String.format("Creating Import Job - " + fileImportJob));
        logger.info("File import uploaded: " + fileMetaData.getFileName());

        //Get import file bytes
        String importFileName = fileMetaData.getFileName();
        byte[] importFileBytes = null;
        try {
            importFileBytes = IOUtils.toByteArray(fileInputStream);
            logger.info("File import size: " + importFileBytes.length);
        } catch (IOException e) {
            e.printStackTrace();
            return Response.serverError().entity(String.format("There was a reading uploaded import file")).build();
        }

        //create import job
        ImportJobBean importJobBean = (ImportJobBean) JSONUtil.fromJSONToObject(fileImportJob, ImportJobBean.class);
        ImportJob importJob = null;
        try {
            importJob = importJobService.saveImportJob(importJobBean, importFileName, importFileBytes);
        } catch (ServiceException e) {
            e.printStackTrace();
            return Response.serverError().entity(String.format("There was a problem creating import job '%s' on the DB.", importJobBean.getImportJobName())).build();
        }

        //convert to bean
        ImportJobBean importJobBeanPersisted = null;
        try {
            importJobBeanPersisted = importJobBeanMapper.map(importJob);
        } catch (MapperException e) {
            e.printStackTrace();
        }

        //send event
        try {
            importJobEventsProducer.fireImportJobCreatedEvent(importJobBeanPersisted);
        } catch (EventProducerException e) {
            e.printStackTrace();
        }

        String jsonString = JSONUtil.toJson(importJobBeanPersisted);
        return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateImportJob() {
        logger.info("Update Import Job Operation invoked");

        ImportJob importJob = new ImportJob();
        importJob.setImportJobId(30010000999L);
        //importJob.setSysTaskId(6005345L);
        importJob.setName("BP Import Job Just Updated");

        String jsonString = JSONUtil.toJson(importJob);
        return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
    }

    @DELETE
    @Path("/{importJobId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteCountry(@PathParam("importJobId") int importJobId) {
        logger.info("Delete Import Job Operation invoked");

        return Response.ok(String.format("Import Job deleted successfully [%d]", importJobId), MediaType.APPLICATION_JSON).build();
    }
}
