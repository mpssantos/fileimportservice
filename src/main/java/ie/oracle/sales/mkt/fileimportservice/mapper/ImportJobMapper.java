package ie.oracle.sales.mkt.fileimportservice.mapper;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 * Created by usermpss on 17/06/2016.
 */
@Component
public class ImportJobMapper implements Mapper<ImportJob, ImportJobBean> {

    @Override
    public ImportJob map(ImportJobBean importJobBean) throws MapperException {
        Objects.requireNonNull(importJobBean, "ImportJobBean is required");
        return mapImportJobBean(importJobBean);
    }

    private ImportJob mapImportJobBean(ImportJobBean importJobBean) throws MapperException {
        ImportJob importJob = new ImportJob();

        importJob.setName(importJobBean.getImportJobName());
        importJob.setDescText(importJobBean.getDescription());
        importJob.setObjectTypeCD(importJobBean.getObjectType());

        importJob.setCreatedBy("SALES_ADMIN");
        importJob.setCreationDate(Calendar.getInstance());

        importJob.setLastUpdateBy("SALES_ADMIN");
        importJob.setLastUpdateDate(Calendar.getInstance());
        importJob.setLastUpdateLogin("SALES_ADMIN");

        importJob.setObjectVersionNumber(1);

        return importJob;
    }

    @Override
    public List<ImportJob> mapList(List<ImportJobBean> input) throws MapperException {
        return null;
    }

}
