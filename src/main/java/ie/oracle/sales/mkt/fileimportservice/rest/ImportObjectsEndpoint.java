package ie.oracle.sales.mkt.fileimportservice.rest;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportObjectBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportObject;
import ie.oracle.sales.mkt.fileimportservice.mapper.ImportObjectBeanMapper;
import ie.oracle.sales.mkt.fileimportservice.mapper.MapperException;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportObjectRepository;
import ie.oracle.sales.salescloudutils.rest.JSONUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by mapasant on 11/11/2016.
 */
@Component
@Path("/importobjects")
public class ImportObjectsEndpoint {
    private static Logger logger = Logger.getLogger(ImportObjectsEndpoint.class);

    @Autowired
    private ImportObjectRepository fileImportObjectRepository;

    @Autowired
    private ImportObjectBeanMapper fileImportObjectBeanMapper;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getImportJobs() {
        logger.info("Get Import Jobs List operation invoked");
        List<ImportObject> importJobList = fileImportObjectRepository.findFileImportObjectByActive(true);

        try {
            List<ImportObjectBean> fileImportObjectBeanList = fileImportObjectBeanMapper.mapList(importJobList);
            String jsonString = JSONUtil.toJson(fileImportObjectBeanList);
            return Response.ok(jsonString, MediaType.APPLICATION_JSON).build();
        } catch (MapperException e) {
            e.printStackTrace();
        }

        return Response.serverError().entity("There was a problem retrieving File Import Objects list").build();
    }
}
