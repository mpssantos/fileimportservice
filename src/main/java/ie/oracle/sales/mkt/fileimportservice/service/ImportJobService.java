package ie.oracle.sales.mkt.fileimportservice.service;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJobActivity;
import ie.oracle.sales.mkt.fileimportservice.enums.ImportJobStatusEnum;
import ie.oracle.sales.mkt.fileimportservice.mapper.ImportJobMapper;
import ie.oracle.sales.mkt.fileimportservice.mapper.MapperException;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportJobActivityRepository;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportJobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Created by mapasant on 09/11/2016.
 */
@Service
public class ImportJobService {
    @Autowired
    private ImportJobRepository importJobRepository;

    @Autowired
    private ImportJobActivityRepository importJobActivityRepository;

    @Autowired
    private ImportJobMapper importJobMapper;

    public List<ImportJob> findAllImportJobs() {
        List<ImportJob> importJobList = importJobRepository.findAllByOrderByLastUpdateDateDesc();

        return importJobList;
    }


    public ImportJob saveImportJob(ImportJobBean importJobBean, String importFileName, byte[] importFileContent) throws ServiceException {
        Objects.requireNonNull(importJobBean, "importJobBean is required");
        Objects.requireNonNull(importFileName, "importFileName is required");
        Objects.requireNonNull(importFileContent, "importFileContent is required");

        //Create Import Job
        ImportJob importJob = null;
        try {
            importJob = importJobMapper.map(importJobBean);
        } catch (MapperException e) {
            e.printStackTrace();
            throw new ServiceException("There was a problem mapping Import Job from ImportJobBean.", e);
        }

        importJob.setScheduleStatusCD(ImportJobStatusEnum.NEW.statusKey());

        //Create Import Job Activity
        ImportJobActivity importJobActivity = createImportJobActivity(importJob, importFileName, importFileContent);
        importJob.getImportJobActivities().add(importJobActivity);

        importJob = importJobRepository.save(importJob);

        return importJob;
    }

    public ImportJobActivity createImportJobActivity(ImportJob importJob, String importFileName, byte[] importFileContent) {
        ImportJobActivity importJobActivity = new ImportJobActivity();

        importJobActivity.setImportJob(importJob);
        importJobActivity.setSourceFileName(importFileName);
        importJobActivity.setSourceFileContent(importFileContent);

        return importJobActivity;
    }
}
