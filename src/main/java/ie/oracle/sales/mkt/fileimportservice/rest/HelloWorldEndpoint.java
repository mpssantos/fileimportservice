package ie.oracle.sales.mkt.fileimportservice.rest;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by mapasant on 07/10/2016.
 */
@Component
@Path("helloworld")
public class HelloWorldEndpoint {
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String sayHello() {
        return "Hello World. File Import Service Up & Running!";
    }
}
