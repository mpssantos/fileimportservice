package ie.oracle.sales.mkt.fileimportservice.config;

import ie.oracle.sales.mkt.fileimportservice.rest.ImportObjectsEndpoint;
import ie.oracle.sales.mkt.fileimportservice.rest.HelloWorldEndpoint;
import ie.oracle.sales.mkt.fileimportservice.rest.ImportJobEndPoint;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * Created by msantos on 04/07/2016.
 */
@Component
@ApplicationPath("/rest")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        registerEndpoints();
    }

    private void registerEndpoints() {
        register(MultiPartFeature.class);
        register(HelloWorldEndpoint.class);
        register(ImportJobEndPoint.class);
        register(ImportObjectsEndpoint.class);
    }
}