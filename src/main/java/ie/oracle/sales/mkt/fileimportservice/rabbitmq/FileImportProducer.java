package ie.oracle.sales.mkt.fileimportservice.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by mapasant on 02/08/2016.
 */
@Component
public class FileImportProducer {
    private static Logger logger = Logger.getLogger(FileImportProducer.class);

    private static final String EXCHANGE_NAME = "fileimport";

    @Autowired
    private ConnectionFactoryService connectionFactoryService;

    public void sendMessage(String message, String routingKey) throws Exception {
        Connection connection = connectionFactoryService.createNewConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic");

        channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
        logger.info(" [x] Sent '" + routingKey + "':'" + message + "'");

        connection.close();
    }
}
