package ie.oracle.sales.mkt.fileimportservice.repository;

import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by mapasant on 09/11/2016.
 */
@Transactional
public interface ImportJobRepository extends CrudRepository<ImportJob, Long> {
    public List<ImportJob> findAll();
    public List<ImportJob> findAllByOrderByLastUpdateDateDesc();
    public ImportJob findImportJobByImportJobId(Long importJobId);
}
