package ie.oracle.sales.mkt.fileimportservice.rabbitmq;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobBean;
import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobEventBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportObject;
import ie.oracle.sales.mkt.fileimportservice.mapper.ImportJobEventBeanMapper;
import ie.oracle.sales.mkt.fileimportservice.mapper.MapperException;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportObjectRepository;
import ie.oracle.sales.salescloudutils.rest.JSONUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by mapasant on 11/11/2016.
 */
@Component
public class ImportJobEventsProducer {
    public static final String FILE_IMPORT_CREATED_EVENT_BINDING_KEY = "fileimport.%s.created";

    private static Logger logger = Logger.getLogger(FileImportProducer.class);

    @Autowired
    private ImportObjectRepository importObjectRepository;

    @Autowired
    private FileImportProducer fileImportProducer;

    @Autowired
    private ImportJobEventBeanMapper importJobCreatedEventBeanMapper;

    public void fireImportJobCreatedEvent(ImportJobBean importJobBean) throws EventProducerException {
        ImportJobEventBean importJobCreatedEventBean = null;
        try {
            importJobCreatedEventBean = importJobCreatedEventBeanMapper.map(importJobBean);
        } catch (MapperException e) {
            e.printStackTrace();
            throw new EventProducerException(String.format("There was a problem mapping to importJobCreatedEventBean: ImportJobId - %d; BatchId - %d", importJobBean.getImportJobId(), importJobBean.getSysTaskId()), e);
        }

        String importJobCreatedEventBeanJSON = JSONUtil.toJson(importJobCreatedEventBean);

        //Binding key
        String bindingKey = createBindingKey(FILE_IMPORT_CREATED_EVENT_BINDING_KEY, importJobBean.getObjectType());
        try {
            logger.info(String.format("Firing Import Job Creation Event: BindingKey - %s; ImportJobId - %d; BatchId - %d", bindingKey, importJobCreatedEventBean.getImportJobId(), importJobCreatedEventBean.getBatchId()));
            fileImportProducer.sendMessage(importJobCreatedEventBeanJSON, bindingKey);
        } catch (Exception e) {
            e.printStackTrace();
            throw new EventProducerException(String.format("There was a problem mapping to importJobCreatedEventBean: ImportJobId - %d; BatchId - %d", importJobBean.getImportJobId(), importJobBean.getSysTaskId()), e);
        }
    }

    private String createBindingKey(String bindingKeyPattern, String objectType) {
        ImportObject importObject = importObjectRepository.findFileImportObjectByObjectType(objectType);
        String objectBindingKey = importObject.getQueueBindingKey();
        String bindingKey = String.format(bindingKeyPattern, objectBindingKey);

        return bindingKey;
    }
}
