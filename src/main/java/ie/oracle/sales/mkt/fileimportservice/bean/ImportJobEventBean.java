package ie.oracle.sales.mkt.fileimportservice.bean;

/**
 * Created by mapasant on 11/11/2016.
 */
public class ImportJobEventBean {
    private Long importJobId;
    private Long batchId;
    private String objectType;

    public Long getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Long importJobId) {
        this.importJobId = importJobId;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    @Override
    public String toString() {
        return "ImportJobCreatedEventBean{" +
                "importJobId=" + importJobId +
                ", batchId=" + batchId +
                ", objectType='" + objectType + '\'' +
                '}';
    }
}
