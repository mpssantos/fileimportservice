package ie.oracle.sales.mkt.fileimportservice.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by mapasant on 03/11/2016.
 */
@Component
public class EnvironmentConfig {
    @Value("${server.hostname}")
    private String hostName;

    @Value("${server.port}")
    private int serverPort;

    public String getHostName() {
        return hostName;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getLocalHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getLocalHostNameAddress() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return null;
    }
}
