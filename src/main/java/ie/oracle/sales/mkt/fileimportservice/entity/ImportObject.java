package ie.oracle.sales.mkt.fileimportservice.entity;

import javax.persistence.*;

/**
 * Created by mapasant on 11/11/2016.
 */
@Entity
@Table(name="IMPORT_OBJECTS")
public class ImportObject {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "OBJECT_TYPE")
    private String objectType;

    @Column(name = "OBJECT_TYPE_LABEL")
    private String objectTypeLabel;

    @Column(name = "QUEUE_BINDING_KEY")
    private String queueBindingKey;

    @Column(name = "ACTIVE")
    private Boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectTypeLabel() {
        return objectTypeLabel;
    }

    public void setObjectTypeLabel(String objectTypeLabel) {
        this.objectTypeLabel = objectTypeLabel;
    }

    public String getQueueBindingKey() {
        return queueBindingKey;
    }

    public void setQueueBindingKey(String queueBindingKey) {
        this.queueBindingKey = queueBindingKey;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImportObject that = (ImportObject) o;

        if (!id.equals(that.id)) return false;
        return objectType.equals(that.objectType);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + objectType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "FileImportObject{" +
                "id=" + id +
                ", objectType='" + objectType + '\'' +
                ", objectTypeLabel='" + objectTypeLabel + '\'' +
                ", queueBindingKey='" + queueBindingKey + '\'' +
                ", active=" + active +
                '}';
    }
}
