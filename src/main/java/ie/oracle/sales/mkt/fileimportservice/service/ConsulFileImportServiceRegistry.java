package ie.oracle.sales.mkt.fileimportservice.service;

import ie.oracle.sales.mkt.fileimportservice.config.EnvironmentConfig;
import ie.oracle.sales.salescloudutils.consul.beans.ServiceConsulConfigBean;
import ie.oracle.sales.salescloudutils.consul.beans.ServiceConsulConfigCheckBean;
import ie.oracle.sales.salescloudutils.rest.JSONUtil;
import ie.oracle.sales.salescloudutils.rest.RESTUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by mapasant on 05/07/2016.
 */
@Component
public class ConsulFileImportServiceRegistry implements ApplicationListener<ApplicationReadyEvent> {
    private static Logger logger = Logger.getLogger(ConsulFileImportServiceRegistry.class);

    @Value("${server.port}")
    private String serverPort;

    @Value("${consul.url}")
    private String consulURL;

    @Value("${server.servicename}")
    private String serviceName;

    @Autowired
    private EnvironmentConfig environmentConfig;

    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        logger.info("**** REGISTERING DEAL PUBLIC SERVICE ON CONSUL ****");

        ServiceConsulConfigBean serviceConsulConfigBean = createServiceConsulConfig();
        String jsonString = JSONUtil.toJson(serviceConsulConfigBean);
        logger.info("**** Service Consul config: " + jsonString);

        logger.info("**** Registering on Consul server listening on " + consulURL);
        String serviceRegistryResponse = null;
        try {
            serviceRegistryResponse = RESTUtil.httpJSONRESTRequestPOST(consulURL, "/v1/agent/service/register", jsonString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(serviceRegistryResponse != null) {
            logger.info("**** I AM Registered under " + serviceConsulConfigBean.getName() + "*****************");
            logger.info("**** Consul Response: " + serviceRegistryResponse);
        }
    }

    private ServiceConsulConfigBean createServiceConsulConfig() {
        ServiceConsulConfigBean serviceConfig = new ServiceConsulConfigBean();
        serviceConfig.setId(serviceName);
        serviceConfig.setName(serviceName);

        //logger.info(String.format("InetAddress HostName: %s", environmentConfig.getLocalHostName()));
        //logger.info(String.format("InetAddress HostNameAddress: %s", environmentConfig.getLocalHostNameAddress()));

        String hostAddress = environmentConfig.getHostName();
        int hostPort = environmentConfig.getServerPort();

        serviceConfig.setAddress(hostAddress);
        serviceConfig.setPort(hostPort);

        ServiceConsulConfigCheckBean check = new ServiceConsulConfigCheckBean();
        check.setHttp(String.format("http://%s:%s/%s/rest/helloworld", serviceConfig.getAddress(), serviceConfig.getPort(),serviceConfig.getName()));
        check.setInterval("10s");
        check.setTimeout("5s");

        serviceConfig.setCheck(check);

        return serviceConfig;
    }

    public void setEnvironmentConfig(EnvironmentConfig environmentConfig) {
        this.environmentConfig = environmentConfig;
    }
}
