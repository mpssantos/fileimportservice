package ie.oracle.sales.mkt.fileimportservice.repository;

import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJobActivity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by mapasant on 10/11/2016.
 */
@Transactional
public interface ImportJobActivityRepository extends CrudRepository<ImportJobActivity, Long> {
    public ImportJobActivity findImportJobActivityByBatchId(Long batchId);

    @Query("SELECT jobActivity FROM ImportJobActivity jobActivity WHERE jobActivity.importJob.importJobId = :importJobId ORDER BY jobActivity.creationDate DESC")
    public List<ImportJobActivity> findImportJobActivitiesByImportJobId(@Param("importJobId") Long importJobId);

    //public ImportJob findImportJobActivityByImportJobId(Long importJobId);
}
