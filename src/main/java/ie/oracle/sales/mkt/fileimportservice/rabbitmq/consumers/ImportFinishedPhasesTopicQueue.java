package ie.oracle.sales.mkt.fileimportservice.rabbitmq.consumers;

import com.rabbitmq.client.*;
import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobEventBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import ie.oracle.sales.mkt.fileimportservice.enums.ImportJobStatusEnum;
import ie.oracle.sales.mkt.fileimportservice.rabbitmq.ConnectionFactoryService;
import ie.oracle.sales.mkt.fileimportservice.rabbitmq.FileImportProducer;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportJobErrorRepository;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportJobRepository;
import ie.oracle.sales.salescloudutils.rest.JSONUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeoutException;

/**
 * Created by mapasant on 11/11/2016.
 */
@Component
public class ImportFinishedPhasesTopicQueue implements InitializingBean, DisposableBean {
    private static Logger logger = Logger.getLogger(ImportFinishedPhasesTopicQueue.class);

    private static final String EXCHANGE_NAME = "fileimport";

    private static final String PRELOAD_FINISHED_BINDING_KEY = "fileimport.*.preload.finished";
    private static final String ENRICHMENT_FINISHED_BINDING_KEY = "fileimport.*.enrichment.finished";
    private static final String VALIDATION_FINISHED_BINDING_KEY = "fileimport.*.validation.finished";
    private static final String POST_LOAD_FINISHED_BINDING_KEY = "fileimport.*.postload.finished";
    private static final String[] BINDING_KEYS = {PRELOAD_FINISHED_BINDING_KEY, ENRICHMENT_FINISHED_BINDING_KEY, VALIDATION_FINISHED_BINDING_KEY, POST_LOAD_FINISHED_BINDING_KEY};

    private static final String QUEUE_NAME = "Import Phases Finished";
    private static final String QUEUE_NAME_DESC = "Import Phases Finished Topic Queue";

    @Autowired
    private ConnectionFactoryService connectionFactoryService;

    @Autowired
    private FileImportProducer fileImportProducer;

    @Autowired
    private ImportJobRepository importJobRepository;

    @Autowired
    private ImportJobErrorRepository importJobErrorRepository;

    private Connection connection = null;
    private Channel channel = null;

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info(String.format("*** %s: Init method after properties are set ***", QUEUE_NAME_DESC));
        startingConsumerQueue();
    }

    private void startingConsumerQueue() throws IOException, TimeoutException {
        connection = connectionFactoryService.createNewConnection();

        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");

        //String queueName = channel.queueDeclare().getQueue();
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);//boolean durable, boolean exclusive, boolean autoDelete
        for(String bindingKey: BINDING_KEYS) {
            channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, bindingKey);
        }

        logger.info(String.format("[*] %s - Waiting for messages", QUEUE_NAME_DESC));

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyAsString = new String(body, "UTF-8");
                String routingKey = envelope.getRoutingKey();
                logger.info("[x] Received '" + routingKey + "':'" + bodyAsString + "'");

                //transforming body message from json to bean
                ImportJobEventBean importJobEventBean = (ImportJobEventBean) JSONUtil.fromJSONToObject(bodyAsString, ImportJobEventBean.class);

                String phaseName = getPhaseName(routingKey);
                String objectType = importJobEventBean.getObjectType();
                Long importJobId = importJobEventBean.getImportJobId();
                Long batchId = importJobEventBean.getBatchId();

                logger.info(String.format("[x] %s of object %s finished: ImportJobId - %s; BatchId - %s", phaseName, objectType, importJobId, batchId));

                updateImportJobStatus(importJobEventBean, routingKey);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

    private String getPhaseName(String routingKey) {
        if(routingKey.endsWith("preload.finished")) {
            return "preload";
        } else if(routingKey.endsWith("enrichment.finished")) {
            return "enrichment";
        } else if(routingKey.endsWith("validation.finished")) {
            return "validation";
        } else if(routingKey.endsWith("postload.finished")) {
            return "postload";
        }
        return null;
    }

    private void updateImportJobStatus(ImportJobEventBean importJobEventBean, String routingKey) {
        ImportJob importJob = importJobRepository.findImportJobByImportJobId(importJobEventBean.getImportJobId());

        if(routingKey.endsWith("preload.finished")) {
            importJob.setScheduleStatusCD(ImportJobStatusEnum.PRE_LOAD_COMPLETED.statusKey());
        } else if(routingKey.endsWith("enrichment.finished")) {
            importJob.setScheduleStatusCD(ImportJobStatusEnum.ENRICHMENT_COMPLETED.statusKey());
        } else if(routingKey.endsWith("validation.finished")) {
            importJob.setScheduleStatusCD(ImportJobStatusEnum.VALIDATION_COMPLETED.statusKey());
        } else if(routingKey.endsWith("postload.finished")) {
            Long errorCount =importJobErrorRepository .countByBatchId(importJobEventBean.getBatchId());
            if(errorCount == 0) {
                importJob.setScheduleStatusCD(ImportJobStatusEnum.COMPLETED.statusKey());
            } else {
                importJob.setScheduleStatusCD(ImportJobStatusEnum.COMPLETED_WITH_ERRORS.statusKey());
            }
        } else {
            return;
        }

        importJob.setLastUpdateBy("SALES_ADMIN");
        importJob.setLastUpdateBy("SALES_ADMIN");
        importJob.setLastUpdateDate(Calendar.getInstance());
        importJobRepository.save(importJob);
    }

    @Override
    public void destroy() throws Exception {
        logger.info(String.format("*** %s about to be destroyed ***", QUEUE_NAME_DESC));

        if(connection != null) {
            connection.close();
        }

        if(channel != null) {
            channel.close();
        }

        logger.info(String.format("*** %s destroyed sucessfuly ***", QUEUE_NAME_DESC));
    }
}
