package ie.oracle.sales.mkt.fileimportservice.bean;

import javax.persistence.Column;

/**
 * Created by mapasant on 11/11/2016.
 */
public class ImportObjectBean {
    private String objectType;
    private String objectTypeLabel;
    private String queueBindingKey;
    private Boolean active;

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectTypeLabel() {
        return objectTypeLabel;
    }

    public void setObjectTypeLabel(String objectTypeLabel) {
        this.objectTypeLabel = objectTypeLabel;
    }

    public String getQueueBindingKey() {
        return queueBindingKey;
    }

    public void setQueueBindingKey(String queueBindingKey) {
        this.queueBindingKey = queueBindingKey;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
