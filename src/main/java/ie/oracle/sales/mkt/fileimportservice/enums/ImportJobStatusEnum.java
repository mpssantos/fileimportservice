package ie.oracle.sales.mkt.fileimportservice.enums;

/**
 * Created by mapasant on 11/11/2016.
 */
public enum ImportJobStatusEnum {
    COMPLETED("COMPLETED", "Completed"),
    COMPLETE_WITH_WARNINGS("COMPLETE_WITH_WARNINGS", "Completed with warnings"),
    COMPLETED_WITH_ERRORS("COMPLETED_WITH_ERRORS", "Completed with errors"),
    NEW("NEW", "New"),
    ERROR("ERROR", "Error"),
    SCHEDULED("SCHEDULED", "Scheduled"),
    //PROCESSING("PROCESSING", "Processing"),

    PRE_LOADING("PRE_LOADING", "Pre Loading"),
    PRE_LOAD_COMPLETED("PRE_LOAD_COMPLETED", "Pre Load Completed"),
    ENRICHING("ENRICHING", "Enriching"),
    ENRICHMENT_COMPLETED("ENRICHMENT_COMPLETED", "Enrichment Completed"),
    VALIDATING("VALIDATING", "Validating"),
    VALIDATION_COMPLETED("VALIDATION_COMPLETED", "Validation Completed"),
    POST_LOADING("POST_LOADING", "Post Loading"),
    POST_LOAD_COMPLETED("POST_LOAD_COMPLETED", "Post Load Completed");

    //BASE_TABLE_UPLOAD("BASE_TABLE_UPLOAD", "Base Table Upload");

    private String statusKey;
    private String statusText;

    ImportJobStatusEnum(String statusKey, String statusText) {
        this.statusKey = statusKey;
        this.statusText = statusText;
    }

    public String statusKey() {
        return statusKey;
    }

    public String statusText() {
        return statusText;
    }
}
