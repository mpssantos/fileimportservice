package ie.oracle.sales.mkt.fileimportservice.mapper;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportObjectBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportObject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by usermpss on 17/06/2016.
 */
@Component
public class ImportObjectBeanMapper implements Mapper<ImportObjectBean, ImportObject> {

    @Override
    public ImportObjectBean map(ImportObject fileImportObject) throws MapperException {
        Objects.requireNonNull(fileImportObject, "FileImportObject is required");
        return mapImportJobBean(fileImportObject);
    }

    private ImportObjectBean mapImportJobBean(ImportObject fileImportObject) throws MapperException {
        ImportObjectBean fileImportObjectBean = new ImportObjectBean();

        fileImportObjectBean.setObjectType(fileImportObject.getObjectType());
        fileImportObjectBean.setObjectTypeLabel(fileImportObject.getObjectTypeLabel());
        fileImportObjectBean.setQueueBindingKey(fileImportObject.getQueueBindingKey());
        fileImportObjectBean.setActive(fileImportObject.getActive());

        return fileImportObjectBean;
    }

    @Override
    public List<ImportObjectBean> mapList(List<ImportObject> fileImportObjectList) throws MapperException {
        List<ImportObjectBean> fileImportObjectBeanList = new ArrayList<>();

        for(ImportObject fileImportObject: fileImportObjectList) {
            ImportObjectBean fileImportObjectBean = map(fileImportObject);
            fileImportObjectBeanList.add(fileImportObjectBean);
        }

        return fileImportObjectBeanList;
    }

}
