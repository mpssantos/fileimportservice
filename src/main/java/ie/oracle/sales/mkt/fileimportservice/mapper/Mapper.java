package ie.oracle.sales.mkt.fileimportservice.mapper;

import java.util.List;

/**
 * Created by usermpss on 17/06/2016.
 */
public interface Mapper<T, K> {
    public T map(K input) throws MapperException;
    public List<T> mapList(List<K> input) throws MapperException;
}
