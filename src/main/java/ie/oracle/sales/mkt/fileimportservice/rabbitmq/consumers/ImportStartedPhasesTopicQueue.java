package ie.oracle.sales.mkt.fileimportservice.rabbitmq.consumers;

import com.rabbitmq.client.*;
import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobEventBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import ie.oracle.sales.mkt.fileimportservice.enums.ImportJobStatusEnum;
import ie.oracle.sales.mkt.fileimportservice.rabbitmq.ConnectionFactoryService;
import ie.oracle.sales.mkt.fileimportservice.rabbitmq.FileImportProducer;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportJobRepository;
import ie.oracle.sales.salescloudutils.rest.JSONUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeoutException;

/**
 * Created by mapasant on 11/11/2016.
 */
@Component
public class ImportStartedPhasesTopicQueue implements InitializingBean, DisposableBean {
    private static Logger logger = Logger.getLogger(ImportStartedPhasesTopicQueue.class);

    private static final String EXCHANGE_NAME = "fileimport";
    private static final String[] BINDING_KEYS = {"fileimport.*.preload.started", "fileimport.*.enrichment.started", "fileimport.*.validation.started", "fileimport.*.postload.started"};

    private static final String QUEUE_NAME = "Import.Phases.Started";
    private static final String QUEUE_NAME_DESC = "Import Phases Started Topic Queue";

    @Autowired
    private ConnectionFactoryService connectionFactoryService;

    @Autowired
    private FileImportProducer fileImportProducer;

    @Autowired
    private ImportJobRepository importJobRepository;

    private Connection connection = null;
    private Channel channel = null;

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info(String.format("*** %s: Init method after properties are set ***", QUEUE_NAME_DESC));
        startingConsumerQueue();
    }

    private void startingConsumerQueue() throws IOException, TimeoutException {
        connection = connectionFactoryService.createNewConnection();

        channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");

        //String queueName = channel.queueDeclare().getQueue();
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);//boolean durable, boolean exclusive, boolean autoDelete
        for(String bindingKey: BINDING_KEYS) {
            channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, bindingKey);
        }

        logger.info(String.format("[*] %s - Waiting for messages", QUEUE_NAME_DESC));

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String bodyAsString = new String(body, "UTF-8");
                String routingKey = envelope.getRoutingKey();
                logger.info("[x] Received '" + routingKey + "':'" + bodyAsString + "'");

                //transforming body message from json to bean
                ImportJobEventBean importJobEventBean = (ImportJobEventBean) JSONUtil.fromJSONToObject(bodyAsString, ImportJobEventBean.class);
                String phaseName = getPhaseName(routingKey);
                logger.info(String.format("[x] %s of object %s started: ImportJobId - %s; BatchId - %s", phaseName, importJobEventBean.getObjectType(), importJobEventBean.getImportJobId(), importJobEventBean.getBatchId()));

                setImportJobStatus(importJobEventBean, routingKey);
            }
        };
        channel.basicConsume(QUEUE_NAME, true, consumer);
    }

    private String getPhaseName(String routingKey) {
        if(routingKey.endsWith("preload.started")) {
            return "preload";
        } else if(routingKey.endsWith("enrichment.started")) {
            return "enrichment";
        } else if(routingKey.endsWith("validation.started")) {
            return "validation";
        } else if(routingKey.endsWith("postload.started")) {
            return "postload";
        }

        return null;
    }

    private void setImportJobStatus(ImportJobEventBean importJobEventBean, String routingKey) {
        ImportJob importJob = importJobRepository.findImportJobByImportJobId(importJobEventBean.getImportJobId());

        if(routingKey.endsWith("preload.started")) {
            importJob.setScheduleStatusCD(ImportJobStatusEnum.PRE_LOADING.statusKey());
        } else if(routingKey.endsWith("enrichment.started")) {
            importJob.setScheduleStatusCD(ImportJobStatusEnum.ENRICHING.statusKey());
        } else if(routingKey.endsWith("validation.started")) {
            importJob.setScheduleStatusCD(ImportJobStatusEnum.VALIDATING.statusKey());
        } else if(routingKey.endsWith("postload.started")) {
            importJob.setScheduleStatusCD(ImportJobStatusEnum.POST_LOADING.statusKey());
        }

        importJob.setLastUpdateBy("SALES_ADMIN");
        importJob.setLastUpdateBy("SALES_ADMIN");
        importJob.setLastUpdateDate(Calendar.getInstance());
        importJobRepository.save(importJob);
    }

    @Override
    public void destroy() throws Exception {
        logger.info(String.format("*** %s about to be destroyed ***", QUEUE_NAME_DESC));

        if(connection != null) {
            connection.close();
        }

        if(channel != null) {
            channel.close();
        }

        logger.info(String.format("*** %s destroyed sucessfuly ***", QUEUE_NAME_DESC));
    }
}
