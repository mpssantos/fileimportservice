package ie.oracle.sales.mkt.fileimportservice.mapper;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobBean;
import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobEventBean;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * Created by usermpss on 17/06/2016.
 */
@Component
public class ImportJobEventBeanMapper implements Mapper<ImportJobEventBean, ImportJobBean> {

    @Override
    public ImportJobEventBean map(ImportJobBean importJobBean) throws MapperException {
        Objects.requireNonNull(importJobBean, "ImportJobBean entity is required");
        return mapImportJobBean(importJobBean);
    }

    private ImportJobEventBean mapImportJobBean(ImportJobBean importJobBean) throws MapperException {
        ImportJobEventBean importJobCreatedEventBean = new ImportJobEventBean();

        importJobCreatedEventBean.setImportJobId(importJobBean.getImportJobId());
        importJobCreatedEventBean.setBatchId(importJobBean.getSysTaskId());
        importJobCreatedEventBean.setObjectType(importJobBean.getObjectType());

        return importJobCreatedEventBean;
    }

    @Override
    public List<ImportJobEventBean> mapList(List<ImportJobBean> importJobBeanList) throws MapperException {
        return null;
    }
}
