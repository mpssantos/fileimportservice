package ie.oracle.sales.mkt.fileimportservice.entity;

import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

/**
 * Created by mapasant on 07/10/2016.
 */
@Entity
@Table(name="MKT_IMP_ERRORS")
@DynamicInsert
public class ImportJobError {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ERROR_ID")
    private Long errorId;

    @Column(name = "BATCH_ID")
    private Long batchId;

    @Column(name = "INTERFACE_ROW_ID")
    private Long interfaceRowId;

    @Column(name = "INTERFACE_TABLE_NAME")
    private String interfaceTableName;

    @Column(name = "MESSAGE_NAME")
    private String messageName;

    @Column(name = "MESSAGE_PARAMETERS")
    private String messageParameters;

    @Column(name = "MESSAGE")
    private String message;

    public Long getErrorId() {
        return errorId;
    }

    public void setErrorId(Long errorId) {
        this.errorId = errorId;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public Long getInterfaceRowId() {
        return interfaceRowId;
    }

    public void setInterfaceRowId(Long interfaceRowId) {
        this.interfaceRowId = interfaceRowId;
    }

    public String getInterfaceTableName() {
        return interfaceTableName;
    }

    public void setInterfaceTableName(String interfaceTableName) {
        this.interfaceTableName = interfaceTableName;
    }

    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getMessageParameters() {
        return messageParameters;
    }

    public void setMessageParameters(String messageParameters) {
        this.messageParameters = messageParameters;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImportJobError that = (ImportJobError) o;

        return errorId.equals(that.errorId);

    }

    @Override
    public int hashCode() {
        return errorId.hashCode();
    }

    @Override
    public String toString() {
        return "ImportError{" +
                "errorId=" + errorId +
                ", batchId=" + batchId +
                ", interfaceRowId=" + interfaceRowId +
                ", messageName='" + messageName + '\'' +
                '}';
    }
}
