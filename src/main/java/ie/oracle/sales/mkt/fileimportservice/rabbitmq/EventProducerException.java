package ie.oracle.sales.mkt.fileimportservice.rabbitmq;

/**
 *
 * @author marco.santos
 */
public class EventProducerException extends Exception {

    public EventProducerException() {
    }

    public EventProducerException(String message) {
        super(message);
    }

    public EventProducerException(String message, Throwable cause) {
        super(message, cause);
    }

    public EventProducerException(Throwable cause) {
        super(cause);
    }
}
