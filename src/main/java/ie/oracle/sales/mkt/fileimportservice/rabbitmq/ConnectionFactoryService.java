package ie.oracle.sales.mkt.fileimportservice.rabbitmq;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by msantos on 26/08/2016.
 */
@Component
public class ConnectionFactoryService {
    private static Logger logger = Logger.getLogger(ConnectionFactoryService.class);

    @Value("${spring.rabbitmq.host}")
    private String rabbitHost;

    @Value("${spring.rabbitmq.port}")
    private int rabbitPort;

    public Connection createNewConnection() throws IOException, TimeoutException {
        logger.info(String.format("Creating a connection to %s:%d", rabbitHost, rabbitPort));
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitHost);
        factory.setPort(rabbitPort);

        return factory.newConnection();
    }
}
