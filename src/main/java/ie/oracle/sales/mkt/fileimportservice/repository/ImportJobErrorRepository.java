package ie.oracle.sales.mkt.fileimportservice.repository;

import ie.oracle.sales.mkt.fileimportservice.entity.ImportJobError;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by mapasant on 18/11/2016.
 */
@Transactional
public interface ImportJobErrorRepository extends CrudRepository<ImportJobError, Long> {
    List<ImportJobError> findImportJobErrorByBatchIdAndInterfaceRowId(Long batchId, Long interfaceRowId);
    List<ImportJobError> findImportJobErrorByBatchIdAndInterfaceRowIdAndInterfaceTableName(Long batchId, Long interfaceRowId, String interfaceTableName);
    List<ImportJobError> findImportJobErrorByBatchIdAndInterfaceRowIdAndInterfaceTableNameAndMessageName(Long batchId, Long interfaceRowId, String interfaceTableName, String messageName);

    Long countByBatchId(Long batchId);
}
