package ie.oracle.sales.mkt.fileimportservice.mapper;

/**
 *
 * @author marco.santos
 */
public class MapperException extends Exception {
    
    public MapperException() {
    }

    public MapperException(String message) {
        super(message);
    }

    public MapperException(String message, Throwable cause) {
        super(message, cause);
    }

    public MapperException(Throwable cause) {
        super(cause);
    }
}
