package ie.oracle.sales.mkt.fileimportservice.entity;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by mapasant on 07/10/2016.
 */
@Entity
@Table(name="MKT_IMP_JOBS")
@DynamicInsert
public class ImportJob {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "IMPORT_JOB_ID")
    private Long importJobId;

    @OneToMany (mappedBy="importJob", fetch=FetchType.EAGER)
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    private List<ImportJobActivity> importJobActivities;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESC_TEXT")
    private String descText;

    @Column(name = "OBJECT_TYPE_CD")
    private String objectTypeCD;

    @Column(name = "SCHEDULE_STATUS_CD")
    private String scheduleStatusCD;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name="CREATION_DATE")
    private Calendar creationDate;

    @Column(name="LAST_UPDATED_BY")
    private String lastUpdateBy;

    @Column(name="LAST_UPDATE_DATE")
    private Calendar lastUpdateDate;

    @Column(name="LAST_UPDATE_LOGIN")
    private String lastUpdateLogin;

    @Column(name="OBJECT_VERSION_NUMBER")
    private Integer objectVersionNumber;

    public Long getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Long importJobId) {
        this.importJobId = importJobId;
    }

    public List<ImportJobActivity> getImportJobActivities() {
        if(importJobActivities == null) {
            importJobActivities = new ArrayList<>();
        }

        return importJobActivities;
    }

    public void setImportJobActivities(List<ImportJobActivity> importJobActivities) {
        this.importJobActivities = importJobActivities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescText() {
        return descText;
    }

    public void setDescText(String descText) {
        this.descText = descText;
    }

    public String getObjectTypeCD() {
        return objectTypeCD;
    }

    public void setObjectTypeCD(String objectTypeCD) {
        this.objectTypeCD = objectTypeCD;
    }

    public String getScheduleStatusCD() {
        return scheduleStatusCD;
    }

    public void setScheduleStatusCD(String scheduleStatusCD) {
        this.scheduleStatusCD = scheduleStatusCD;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }

    public Calendar getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Calendar lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getLastUpdateLogin() {
        return lastUpdateLogin;
    }

    public void setLastUpdateLogin(String lastUpdateLogin) {
        this.lastUpdateLogin = lastUpdateLogin;
    }

    public Integer getObjectVersionNumber() {
        return objectVersionNumber;
    }

    public void setObjectVersionNumber(Integer objectVersionNumber) {
        this.objectVersionNumber = objectVersionNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImportJob importJob = (ImportJob) o;

        return importJobId.equals(importJob.importJobId);

    }

    @Override
    public int hashCode() {
        return importJobId.hashCode();
    }

    @Override
    public String toString() {
        return "ImportJob{" +
                "importJobId=" + importJobId +
                ", name='" + name + '\'' +
                '}';
    }
}