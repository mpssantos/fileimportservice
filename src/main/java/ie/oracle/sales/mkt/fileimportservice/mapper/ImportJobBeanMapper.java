package ie.oracle.sales.mkt.fileimportservice.mapper;

import ie.oracle.sales.mkt.fileimportservice.bean.ImportJobBean;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJob;
import ie.oracle.sales.mkt.fileimportservice.entity.ImportJobActivity;
import ie.oracle.sales.mkt.fileimportservice.repository.ImportJobActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by usermpss on 17/06/2016.
 */
@Component
public class ImportJobBeanMapper implements Mapper<ImportJobBean, ImportJob> {

    @Autowired
    private ImportJobActivityRepository importJobActivityRepository;

    @Override
    public ImportJobBean map(ImportJob importJob) throws MapperException {
        Objects.requireNonNull(importJob, "ImportJob entity is required");
        return mapImportJobBean(importJob);
    }

    private ImportJobBean mapImportJobBean(ImportJob importJob) throws MapperException {
        ImportJobBean importJobBean = new ImportJobBean();

        importJobBean.setImportJobId(importJob.getImportJobId());

        if(importJob.getImportJobActivities() != null || !importJob.getImportJobActivities().isEmpty()) {
            List<ImportJobActivity> importJobActivities = importJobActivityRepository.findImportJobActivitiesByImportJobId(importJob.getImportJobId());
            if(!importJobActivities.isEmpty()) {
                importJobBean.setSysTaskId(importJobActivities.get(0).getBatchId());
            }
        }

        importJobBean.setImportJobName(importJob.getName());
        importJobBean.setObjectType(importJob.getObjectTypeCD());
        importJobBean.setDescription(importJob.getDescText());
        importJobBean.setStatus(importJob.getScheduleStatusCD());

        return importJobBean;
    }

    public List<ImportJobBean> mapList(List<ImportJob> importJobList) throws MapperException {
        List<ImportJobBean> importJobBeanList = new ArrayList<>();

        for(ImportJob importJob: importJobList) {
            ImportJobBean importJobBean = map(importJob);
            importJobBeanList.add(importJobBean);
        }

        return importJobBeanList;
    }

}
