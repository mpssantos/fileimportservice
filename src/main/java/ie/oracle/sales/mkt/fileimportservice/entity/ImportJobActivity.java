package ie.oracle.sales.mkt.fileimportservice.entity;

import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by mapasant on 07/10/2016.
 */
@Entity
@Table(name="MKT_IMP_JOB_ACTIVITIES")
@DynamicInsert
public class ImportJobActivity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "BATCH_ID")
    private Long batchId;

    @ManyToOne
    @JoinColumn(name = "IMPORT_JOB_ID")
    private ImportJob importJob;

    @Column(name = "SOURCE_FILE_NAME")
    private String sourceFileName;

    @Column(name = "SOURCE_FILE_TYPE")
    private String sourceFileType;

    @Column(name = "SOURCE_FILE_CONTENT")
    private byte[] sourceFileContent;

    @Column(name = "CREATION_DATE")
    private Calendar creationDate;

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public ImportJob getImportJob() {
        return importJob;
    }

    public void setImportJob(ImportJob importJob) {
        this.importJob = importJob;
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }

    public String getSourceFileType() {
        return sourceFileType;
    }

    public void setSourceFileType(String sourceFileType) {
        this.sourceFileType = sourceFileType;
    }

    public byte[] getSourceFileContent() {
        return sourceFileContent;
    }

    public void setSourceFileContent(byte[] sourceFileContent) {
        this.sourceFileContent = sourceFileContent;
    }

    public Calendar getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Calendar creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImportJobActivity that = (ImportJobActivity) o;

        return batchId.equals(that.batchId);

    }

    @Override
    public int hashCode() {
        return batchId.hashCode();
    }

    @Override
    public String toString() {
        return "ImportJobActivity{" +
                "batchId=" + batchId +
                ", sourceFileName='" + sourceFileName + '\'' +
                ", sourceFileType='" + sourceFileType + '\'' +
                '}';
    }
}
