package ie.oracle.sales.mkt.fileimportservice.bean;

/**
 * Created by mapasant on 09/11/2016.
 */
public class ImportJobBean {
    private Long importJobId;
    private Long sysTaskId;//batchId
    private String importJobName;
    private String description;
    private String objectType;
    private String status;

    public Long getImportJobId() {
        return importJobId;
    }

    public void setImportJobId(Long importJobId) {
        this.importJobId = importJobId;
    }

    public Long getSysTaskId() {
        return sysTaskId;
    }

    public void setSysTaskId(Long sysTaskId) {
        this.sysTaskId = sysTaskId;
    }

    public String getImportJobName() {
        return importJobName;
    }

    public void setImportJobName(String importJobName) {
        this.importJobName = importJobName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImportJobBean that = (ImportJobBean) o;

        if (!importJobId.equals(that.importJobId)) return false;
        return importJobName.equals(that.importJobName);

    }

    @Override
    public int hashCode() {
        int result = importJobId.hashCode();
        result = 31 * result + importJobName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ImportJobBean{" +
                "importJobId='" + importJobId + '\'' +
                "sysTaskId='" + sysTaskId + '\'' +
                "importJobName='" + importJobName + '\'' +
                ", objectType='" + objectType + '\'' +
                '}';
    }
}
