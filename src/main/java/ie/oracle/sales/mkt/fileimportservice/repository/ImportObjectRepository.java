package ie.oracle.sales.mkt.fileimportservice.repository;

import ie.oracle.sales.mkt.fileimportservice.entity.ImportObject;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by mapasant on 11/11/2016.
 */
@Transactional
public interface ImportObjectRepository extends CrudRepository<ImportObject, Long> {
    public List<ImportObject> findAll();
    public List<ImportObject> findFileImportObjectByActive(boolean active);
    public ImportObject findFileImportObjectByObjectType(String objectType);
}
