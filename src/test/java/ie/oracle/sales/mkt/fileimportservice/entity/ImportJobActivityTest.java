package ie.oracle.sales.mkt.fileimportservice.entity;

import ie.oracle.sales.mkt.fileimportservice.categories.Unit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by mapasant on 07/10/2016.
 */
public class ImportJobActivityTest {
    @Test
    @Category(Unit.class)
    public void testToString() {
        ImportJobActivity importBatchId = new ImportJobActivity();
        importBatchId.setBatchId(30001232L);

        Assert.assertTrue(importBatchId.toString().contains("ImportJobActivity{"));
        Assert.assertTrue(importBatchId.toString().contains("batchId=30001232"));
    }

    @Test
    @Category(Unit.class)
    public void testEquals() {
        ImportJobActivity importBatchId1 = new ImportJobActivity();
        importBatchId1.setBatchId(30001232L);

        ImportJobActivity importBatchId2 = new ImportJobActivity();
        importBatchId2.setBatchId(8500444L);

        Assert.assertTrue(importBatchId1.equals(importBatchId1));
        Assert.assertEquals(importBatchId1, importBatchId1);
        Assert.assertEquals(importBatchId2, importBatchId2);

        Assert.assertFalse(importBatchId1.equals(importBatchId2));
        Assert.assertNotEquals(importBatchId1, importBatchId2);
    }

    @Test
    @Category(Unit.class)
    public void testHashCode() {
        ImportJobActivity importBatchId1 = new ImportJobActivity();
        importBatchId1.setBatchId(30001232L);

        ImportJobActivity importBatchId2 = new ImportJobActivity();
        importBatchId2.setBatchId(30001232L);

        ImportJobActivity importBatchId3 = new ImportJobActivity();
        importBatchId3.setBatchId(300231232L);

        Assert.assertEquals(importBatchId1.hashCode(), importBatchId1.hashCode());
        Assert.assertEquals(importBatchId1.hashCode(), importBatchId2.hashCode());

        Assert.assertNotEquals(importBatchId1.hashCode(), importBatchId3.hashCode());
    }
}
