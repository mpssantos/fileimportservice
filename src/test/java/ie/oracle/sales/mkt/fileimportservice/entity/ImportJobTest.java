package ie.oracle.sales.mkt.fileimportservice.entity;

import ie.oracle.sales.mkt.fileimportservice.categories.Unit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by mapasant on 07/10/2016.
 */
public class ImportJobTest {
    @Test
    @Category(Unit.class)
    public void testToString() {
        ImportJob importJob = new ImportJob();
        importJob.setImportJobId(30001232L);

        Assert.assertTrue(importJob.toString().contains("ImportJob{"));
    }

    @Test
    @Category(Unit.class)
    public void testEquals() {
        ImportJob importJob1 = new ImportJob();
        importJob1.setImportJobId(30001212L);

        ImportJob importJob2 = new ImportJob();
        importJob2.setImportJobId(30001212L);


        Assert.assertTrue(importJob1.equals(importJob1));
        Assert.assertEquals(importJob1, importJob1);
        Assert.assertEquals(importJob2, importJob2);
    }

    @Test
    @Category(Unit.class)
    public void testHashCode() {
        ImportJob importJob1 = new ImportJob();
        importJob1.setImportJobId(30001212L);

        ImportJob importJob2 = new ImportJob();
        importJob2.setImportJobId(30001212L);

        Assert.assertEquals(importJob1.hashCode(), importJob1.hashCode());
        Assert.assertEquals(importJob1.hashCode(), importJob2.hashCode());
    }
}
