package ie.oracle.sales.mkt.fileimportservice.entity;

import ie.oracle.sales.mkt.fileimportservice.categories.Unit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by mapasant on 07/10/2016.
 */
public class ImportJobErrorTest {
    @Test
    @Category(Unit.class)
    public void testToString() {
        ImportJobError importJobError = new ImportJobError();
        importJobError.setErrorId(3000111L);

        Assert.assertTrue(importJobError.toString().contains("ImportError{"));
    }

    @Test
    @Category(Unit.class)
    public void testEquals() {
        ImportJobError importJobError1 = new ImportJobError();
        importJobError1.setErrorId(3000111L);
        importJobError1.setBatchId(3000222L);

        ImportJobError importJobError2 = new ImportJobError();
        importJobError2.setErrorId(3000112L);
        importJobError2.setBatchId(3000223L);


        Assert.assertTrue(importJobError1.equals(importJobError1));
        Assert.assertEquals(importJobError1, importJobError1);
        Assert.assertEquals(importJobError2, importJobError2);

        Assert.assertFalse(importJobError1.equals(importJobError2));
        Assert.assertNotEquals(importJobError1, importJobError2);
    }

    @Test
    @Category(Unit.class)
    public void testHashCode() {
        ImportJobError importJobError1 = new ImportJobError();
        importJobError1.setErrorId(3000111L);
        importJobError1.setBatchId(3000222L);

        ImportJobError importJobError2 = new ImportJobError();
        importJobError2.setErrorId(3000111L);
        importJobError2.setBatchId(3000222L);

        ImportJobError importJobError3 = new ImportJobError();
        importJobError3.setErrorId(3000333L);
        importJobError3.setBatchId(3000234L);


        Assert.assertEquals(importJobError1.hashCode(), importJobError1.hashCode());
        Assert.assertEquals(importJobError1.hashCode(), importJobError2.hashCode());

        Assert.assertNotEquals(importJobError1.hashCode(), importJobError3.hashCode());
    }
}
